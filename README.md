# Installation Silvertool vers Docker/Kubernetes


## Installation MYSQL

Commençons par deployer un container MYSQL.

```bash
docker pull mysql/mysql-server
```

```bash
docker run --name mysql-silvertool \
-p 3306:3306 \
--v mondossierpersistant:/var/lib/mysql \
-e MYSQL_PASSWORD='password user' \
-e MYSQL_ROOT_PASSWORD='password root' \
-e MYSQL_USER='user' mysql/mysql-server
```

## Installation PHPMYADMIN

Deployons ensuite un container phpmyadmin afin de creer une nouvelle base de donnees qui servira à heberger Silvertool

```bash
docker pull phpmyadmin/phpmyadmin
```

```bash
docker run --name phpmyadmin \
-p 8080:80 \
--link mysql-silvertool:db \
-e PMA_HOST=db phpmyadmin
```

## Installation PHP + Apache


```bash
docker pull thelinks/silvertool-docker
```

```bash
docker run --name silvertool \
-p 80:80 \
-p 443:443 \
--link mysql-silvertool:db \
--v mondossierpersistant:/app thelinks/silvertool-docker
```

Les parametres de configuration BDD pour Silvertool seront les suivants:
- TYPEBDD=MYSQL
- HostBDD=db
- PortBDD=3306
- DBName= le nom de la base de donnees cree
- LoginBDD= user indique dans MYSQL_USER lors de l'installation du serveur Mysql
- asswordBDD= mot de passe indique dans MYSQL_PASSWORD lors de l'installation du serveur Mysql

## Customisation

Le fichier "Dockerfile" peut etre modifie pour ajouter, supprimer, changer des variables d'environnements.

Pour modifier la version PHP, l'utilisation de NGINX ou APACHE, modifier cette ligne:

```bash
FROM webdevops/php-apache:7.3
```

Pour cacher les erreurs PHP remplacer: 

```bash
ENV PHP_DISPLAY_ERRORS=1
```

par 

```bash
ENV PHP_DISPLAY_ERRORS=0
```

Retrouvez toutes les versions et variables d'environnement dans l'article de reference [WebDevOps Dockerfiles](https://dockerfile.readthedocs.io/en/latest/)



## TIPS

L'utilisation d'un reverse proxy est fortement conseille
