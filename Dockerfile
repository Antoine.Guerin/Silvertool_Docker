FROM webdevops/php-apache:7.3

# PHP
ENV PHP_DISPLAY_ERRORS=1
ENV PHP_MEMORY_LIMIT=2048M
ENV PHP_MAX_EXECUTION_TIME=3600
ENV PHP_DATE_TIMEZONE="Europe/Paris"
ENV PHP_POST_MAX_SIZE=50000M
ENV PHP_UPLOAD_MAX_FILESIZE=50000M